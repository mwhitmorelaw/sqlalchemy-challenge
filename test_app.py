from unittest import main, TestCase

from sqlalchemy.orm import Session

import app


class TestApp(TestCase):

  def setUp(self):
    self.session = Session(app.engine)
    app.app.config['TESTING'] = True
    self.client = app.app.test_client()

  def tearDown(self):
    self.session.close()

  def test_get_last_date(self):
    self.assertEqual('2017-08-23', app.get_last_date(self.session))

  def test_get_year_before_last_date(self):
    self.assertEqual('2016-08-23', app.get_year_before_last_date(self.session))

  def test_stations(self):
    resp = self.client.get('/api/v1.0/stations')
    stations = resp.json
    self.assertEqual(len(stations), 9)

  def test_get_most_active_last_year(self):
    station = app.get_most_active_last_year(self.session, '2016-08-23')
    self.assertEqual(station, 'USC00519397')

  def test_tobs(self):
    """Query the dates and temperature observations of the most active station for the last year of data.
    Return a JSON list of temperature observations (TOBS) for the previous year."""

    resp = self.client.get('/api/v1.0/tobs')
    data = resp.json
    self.assertEqual(len(data), 352)

  def test_range(self):
    """Return a JSON list of the minimum temperature, the average temperature, and the max temperature for a given start or start-end range."""
    resp = self.client.get('/api/v1.0/2017-08-23')
    data = resp.json

    USC00519397 = [r for r in data if r['station'] == 'USC00519397'][0]
    self.assertEqual(USC00519397['average temp'], 81)
    self.assertEqual(USC00519397['maximum temp'], 81)
    self.assertEqual(USC00519397['minimum temp'], 81)

  def test_range_with_end(self):
    """Return a JSON list of the minimum temperature, the average temperature, and the max temperature for a given start or start-end range."""
    resp = self.client.get('/api/v1.0/2017-08-21/2017-08-22')
    data = resp.json
    USC00519397 = [r for r in data if r['station'] == 'USC00519397'][0]
    self.assertEqual(USC00519397['average temp'], 81.5)
    self.assertEqual(USC00519397['maximum temp'], 82)
    self.assertEqual(USC00519397['minimum temp'], 81)


if __name__ == '__main__':
  main()
