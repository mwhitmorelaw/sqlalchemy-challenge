import datetime

from flask import Flask, jsonify, url_for
import pandas as pd
import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import create_engine, func, and_, desc
from sqlalchemy.orm import Session
from sqlalchemy.types import Date, Numeric, Integer


engine = create_engine("sqlite:///Resources/hawaii.sqlite")

# reflect an existing database into a new model
Base = automap_base()
# reflect the tables
Base.prepare(engine, reflect=True)

Measurement = Base.classes.measurement
Station = Base.classes.station

app = Flask(__name__)


# Home page.
# List all routes that are available.
@app.route('/')
def home_page():
  data = {
    'routes': [
        url_for('precipitation'),
        url_for('stations'),
        url_for('tobs'),
        url_for('start_date', start='start_date'),
        url_for('date_range', start='start_date', end='end_date'),
    ]
  }
  return jsonify(data)


# historical data does not change often. load and cache on start up.
@app.before_first_request
def preprocess():
  session = Session(bind=engine)

  global last_date
  last_date = get_last_date(session)  

  global year_before_last_date
  year_before_last_date = get_year_before_last_date(session)  

  global measurements_last_twelve_months
  measurement_query = preprocess_precipitation(session, last_date)
  measurements_last_twelve_months = jsonify([{r[0]: r[1]} for r in measurement_query.all()])

  global station_list
  station_list_query = preprocess_stations(session)
  station_list = jsonify([r.station for r in station_list_query.all()])

  global busy_station
  busy_station_query = preprocess_tobs(session, last_date)
  busy_station = jsonify(busy_station_query.all())

  session.close()


def get_last_date(session):
  # Calculate the date 1 year ago from the last data point in the database
  # SQLite does not support dates thus casting is not an option. Since all values in date column are in the same date string 
  # format, string comparison can be used. 
  return session.query(func.max(Measurement.date)).first()[0]


def get_year_before_last_date(session):
  last_date = session.query(func.max(Measurement.date)).first()[0]
  twelve_months_ago = datetime.datetime.strptime(last_date, '%Y-%m-%d') - pd.DateOffset(months=12)
  return twelve_months_ago.strftime('%Y-%m-%d')


def preprocess_precipitation(session, last_date):
  # dates are stored as strings thus we need ot confirm all are in the same format. 
  assert session.query(Measurement).filter(~Measurement.date.like('____-__-__')).count() == 0

  # Perform a query to retrieve the data and precipitation scores
  measurements_last_twelve_months = session.query(Measurement.date, Measurement.prcp) \
      .filter(and_(Measurement.prcp != None, Measurement.date > year_before_last_date))

  return measurements_last_twelve_months


def preprocess_stations(session):
  return session.query(Station.station)

def get_most_active_last_year(session, year_ago_date):
  return session.query(
    Measurement.station, 
    func.count(Measurement.id).label('count')) \
    .filter(Measurement.date >= year_ago_date) \
    .group_by(Measurement.station) \
    .order_by(desc(func.count(Measurement.id))).first()[0]

def preprocess_tobs(session, last_date):
  # Query the dates and temperature observations of the most active station for the last year of data.
  # Return a JSON list of temperature observations (TOBS) for the previous year.
  most_active = get_most_active_last_year(session, year_before_last_date)
  measurements_last_twelve_months = session.query(Measurement.date, Measurement.tobs) \
    .filter(and_(Measurement.date >= year_before_last_date, 
                 Measurement.date <= last_date))
  return measurements_last_twelve_months.filter(Measurement.station == 'USC00519281')


# Return a JSON list of the minimum temperature, the average temperature, and the max temperature for a given start or start-end range.
def query_station_stats(session, start, end):
  return session.query(
    Measurement.station, 
    func.count(Measurement.id).label('measurement count'),
    func.min(Measurement.tobs).label('minimum temp'),
    func.max(Measurement.tobs).label('maximum temp'),
    func.avg(Measurement.tobs).label('average temp')) \
    .filter(and_(Measurement.date >= start, Measurement.date <= end)) \
    .group_by(Measurement.station) \
    .order_by(func.count(Measurement.id).desc())  


@app.route('/api/v1.0/precipitation')
def precipitation():
  return measurements_last_twelve_months


@app.route('/api/v1.0/stations')
def stations():
  # Return a JSON list of stations from the dataset.
  return station_list


@app.route('/api/v1.0/tobs')
def tobs():
  return busy_station


@app.route('/api/v1.0/<start>')
def start_date(start):
  # When given the start only, calculate TMIN, TAVG, and TMAX for all dates greater than and equal to the start date.
  session = Session(bind=engine)
  data = query_station_stats(session, start, last_date).all()
  session.close()
  return jsonify(data)


@app.route('/api/v1.0/<start>/<end>')
def date_range(start, end):
  # When given the start and the end date, calculate the TMIN, TAVG, and TMAX for dates between the start and end date inclusive.
  session = Session(bind=engine)
  data = query_station_stats(session, start, end).all()
  session.close()
  return jsonify(data)


if __name__ == '__main__':
    app.run(debug=True)
