# Hawaii Historical Weather Analysis
This project is intended to analyze the historical precipition and and temperature data taken at weather stations in Hawaii. The project includes both a Jupyter Notebook and a Flask application which provide analysis of the included data set. 

## Getting Started

The notebook file can be run in Jupyter with following command line statement: 'jupyter-notebook.exe .\climate.ipynb'.
Once the notebook is open in Jupyter, select the first cell and press SHIFT + ENTER to run it.
This will also move the cell selection to the next cell down. The remainder of the notebook results
can be viewed by continuing to press SHIFT + ENTER in each cell.

The Flask app is contained in the app.py file in the project root directory. The app can be ran with the command 'python app.py'. This will start a debug server at localhost:5000. Navigating to the root directory in a web browser will display the avaliable routes. 

### Prerequisites

This project requires Jupyter Notebook to be installed. Installation instructions can be found [here](https://jupyter.org/install). The project also requires that Flask and SQLAlchemy also be installed. These packages can be downloaded on pip using 'pip install flask, sqlalchemy'


## Running the tests

Automated tests are included in the Jupyter Notebook file in the form of assert statements. None of the 
assert statements should fail as you execute the notebook.
There are also unit tests for the Flask app in project root directory. These unit tests can be run with the command 'python -m unittest' from the project root directory.

## Deployment

The Notebook included in this project is self-contained and intended to be used for one-off runs. Assuming you feel this notebook should be deployed to production, no additional steps are needed beyond those stated in 'Getting Started' above.

As for the Flask app, detailed instructions on deploying a production Flask app can be found [here](https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/)

## Built With

* [Python](http://www.python.org) - Language used
* [Jupyter](https://jupyter.org) - IDE used
* [Pandas](https://pandas.pydata.org) - Data Manipulation Library
* [MatplotLib](https://matplotlib.org) - Data Visualization library
* [SQLAlchemy](https://sqlalchemy.org) - Object Relational Mapper
* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - Web Framework

## Versioning

We use [SemVer](http://semver.org/) for versioning. The version is currently 1.0.0.

## Authors

* **Michael Whitmore** - *All work* - [mwhitmorelaw](https://gitlab.com/mwhitmorelaw)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Guido van Rossum
